<?php

namespace src\helpers;

/**
 * Class Console
 */
class Console
{
    /**
     * FG Colors
     *
     * @var int
     */
    const FG_RED = 31;
    const FG_GREEN = 32;

    /**
     * The command completed successfully.
     */
    const EXIT_OK = 0;

    /**
     * The command exited with an error code that says nothing about the error.
     */
    const EXIT_UNSPECIFIED_ERROR = 1;

    /**
     * @param string $string
     *
     * @return int|false
     */
    public static function stdout(string $string): int|false
    {
        return fwrite(\STDOUT, self::ansiFormat("$string\n", [Console::FG_GREEN]));
    }

    /**
     * @param string $string
     *
     * @return int|false
     */
    public static function stderr(string $string): int|false
    {
        return fwrite(\STDERR, self::ansiFormat("$string\n", [Console::FG_RED]));
    }

    /**
     * @return int|false
     */
    public static function break(): int|false
    {
        return fwrite(\STDOUT, "\n");
    }

    /**
     * @param string $string
     * @param array $format
     *
     * @return string
     */
    public static function ansiFormat(string $string, array $format): string
    {
        $code = implode(';', $format);

        return "\033[0m" . ($code !== '' ? "\033[" . $code . 'm' : '') . $string . "\033[0m";
    }
}
