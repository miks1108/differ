<?php

namespace src;

use Exception;
use src\helpers\Console;

/**
 * Differ will produce two output files:
 * - first output file should contain only strings which were found in first input file,
 *   but not in the second one;
 * - second output file - strings found in the second input file, but not in the first one.
 *
 * Class Differ
 */
class Differ
{
    /**
     * Input/Output files names
     *
     * @var string
     */
    const INPUT_FILE_NAME = 'example/file';
    const OUTPUT_FILE_NAME = 'result';

    /**
     * Start parsing
     *
     * @return bool
     */
    public function exec(): bool
    {
        Console::stdout('Start parsing...');

        try {
            $inputFilename1 = $this->getInputFileName(1);
            $inputFilename2 = $this->getInputFileName(2);
            $array1 = $this->getArrayFromFile($inputFilename1);
            $array2 = $this->getArrayFromFile($inputFilename2);
            Console::stdout('Getting data from files...');

            $result1 = array_diff($array1, $array2);
            $result2 = array_diff($array2, $array1);
            $outputFilename1 = $this->getOutputFileName(1);
            $outputFilename2 = $this->getOutputFileName(2);
            $this->saveDataToFile($result1, $outputFilename1);
            $this->saveDataToFile($result2, $outputFilename2);

            Console::stdout('Saving data to files...');
        } catch (Exception $e) {
            Console::stderr($e->getMessage());

            return Console::EXIT_UNSPECIFIED_ERROR;
        }
        Console::stdout('Done');

        return Console::EXIT_OK;
    }

    /**
     * @param string $filename
     *
     * @return array
     * @throws Exception
     */
    private function getArrayFromFile(string $filename): array
    {
        $content = file_get_contents($filename);
        if (!$content) {
            throw new Exception(
                sprintf('Cannot get data from \'%s\' file', $filename)
            );
        }

        return str_getcsv($content);
    }

    /**
     * @param array $array
     * @param string $filename
     *
     * @return int the number of bytes that were written to the file
     * @throws Exception
     */
    private function saveDataToFile(array $array, string $filename): int
    {
        if (!file_exists($filename) && !touch($filename)) {
            throw new Exception(
                sprintf('Cannot create \'%s\' file', $filename)
            );
        }
        $savedBytes = file_put_contents(
            $filename,
            implode(',', $array)
        );
        if (!$savedBytes) {
            throw new Exception(
                sprintf('Cannot save result to \'%s\' file', $filename)
            );
        }

        return $savedBytes;
    }

    /**
     * @param int $number
     *
     * @return string
     */
    private function getInputFileName(int $number): string
    {
        return $this->getFileName(self::INPUT_FILE_NAME, $number);
    }

    /**
     * @param int $number
     *
     * @return string
     */
    private function getOutputFileName(int $number): string
    {
        return $this->getFileName(self::OUTPUT_FILE_NAME, $number);
    }

    /**
     * @param string $name
     * @param int $number
     *
     * @return string
     */
    private function getFileName(string $name, int $number): string
    {
        return sprintf('%s/%s%d.csv', FILES_DIR, $name, $number);
    }
}
