<?php

use src\Differ;

error_reporting(E_ERROR);

require_once __DIR__ . '/vendor/autoload.php';
defined('BASE_DIR') or define('BASE_DIR', __DIR__);
defined('FILES_DIR') or define('FILES_DIR', BASE_DIR . '/files');

(new Differ())->exec();
