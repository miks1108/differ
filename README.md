<h2>Test</h4>

Please use any well known language (PHP, Perl, C++, Java, python) to develop code which will take two input files, both files consist of lexicographically sorted in the same order ASCII strings, and will produce two output files:
- first output file should contain only strings which were found in first input file, but not in the second one; 
- second output file - strings found in the second input file, but not in the first one.

<h4>To start script:</h4>
<pre>php index.php</pre>
